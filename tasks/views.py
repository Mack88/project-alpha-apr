from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from tasks.models import Task
from projects.models import Project


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.owner = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks_assigned = Task.objects.filter(assignee=request.user)
    context = {
        "tasks_assigned": tasks_assigned,
    }
    return render(request, "tasks/mytasks.html", context)


@login_required
def list_projects(request):
    projects = Project.objects.all()
    context = {
        "projects": projects,
    }
    return render(request, "tasks/list_projects.html", context)
